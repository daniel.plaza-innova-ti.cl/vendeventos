<header>
    <ul class="logo">
        <img src="{{ asset('images/logo.png') }}" alt="logo">
    </ul>
    <ul class="options">
        <li>Quienes somos</li>
        <li>Contacto</li>
        <li>
            <a href="{{ url('home') }}">Iniciar sesion</a>
        </li>
    </ul>
</header>