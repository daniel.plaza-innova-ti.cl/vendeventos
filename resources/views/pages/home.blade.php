@extends('index')

@section('seccion')
    @include('partials.header')
    <div class="texto-marca-container">
        <img class="texto-marca" src="{{asset('images/texto-marca.png')}}" alt="event1image">
        <p>Todos el mundo de eventos en un solo lugar</p>
    </div>
    <div class="slider-home">
        <div class="slide">
            <img src="{{asset('images/event1.jpg')}}" alt="event1image">
        </div>
        <div class="slide">
            <img src="{{asset('images/event2.jpg')}}" alt="event2image">
        </div>
        <div class="slide">
            <img src="{{asset('images/event3.jpg')}}" alt="event3image">
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script>
        $(document).ready(function(){
            $('.slider-home').slick({
                dots: false,
                infinite: true,
                speed: 500,
                fade: true,
                cssEase: 'linear',
                autoplay: true,
                autoplaySpeed: 2000,
                prevArrow: "",
                nextArrow: ""
            });
        });
    </script>
@endsection

