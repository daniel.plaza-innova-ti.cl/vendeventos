@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-3 sidebar-home">
            <ul>
                <li>
                    <a href="{{ url('eventos/create') }}">Crear evento</a>
                </li>
                <li>
                    <a href="{{ url('eventos') }}">Tus eventos</a>
                </li>
                <li>
                    <a href="#">Invitaciones</a>
                </li>
            </ul>
        </div>
        <div class="col-md-9 dashboard">
                <div>Bienvenido {{ Auth::user()->name }}</div>
                <div class="contenido">
                    @yield('home-contenido')
                </div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
        </div>
    </div>

@endsection
